
/* global wp, _ppc */
(function($){
	'use strict';

	if ( 'undefined' === typeof wp.media ) {
		// Let's notify anyone that likes to look into the browser for errors.
		window.console.log('WP.MEDIA is not initialized!');
		return;
	}

	var Photo = Backbone.View.extend({
		el: $( '[name="profile_photo_2"]' ),
		url:		'',
		src:		'',
		photo_id:	'',
		media:		wp.media,
		frame:		false,
		events: {
			'change': 'maybeRemovePhoto'
		},

		initialize: function() {
			this.render();
		},

		render: function() {
			this.browse		= this.$el.next('.browse');
			this.id_holder	= this.$el.siblings('#profile_photo_id');
			this.img_holder	= this.$el.siblings('#profile_photo_holder');
			this.photo_id	= this.id_holder.val();
			this.url		= this.$el.val();

			this.browse.on( 'click', $.proxy( this, 'showMedia', this ) );
			this.$el.trigger( 'change' );
		},

		showMedia: function() {
			if ( false === this.frame ) {
				this.frame = new wp.media( {
					frame:		'select',
					title:		_ppc.title,
					library:	{type:['image']},
					multiple:	false
				} );

				this.frame.on( 'select', this.select, this );
				this.frame.on( 'open', this.set_selection, this );
			}
			this.frame.open();
		},

		select: function() {
			var selection, url, sizes;

			selection = this.frame.state().get('selection').first();
			sizes = selection.get('sizes');
			url = selection.get('url');
			this.photo_id = selection.get('id');
			this.src = url;

			if ( sizes.thumbnail ) {
				// We need the thumbnail to preview the image
				this.src = sizes.thumbnail.url;
			}

			this.$el.val( url );
			this.id_holder.val( this.photo_id );
			this.$el.trigger( 'change', this );
		},

		set_selection: function() {
			var selected, selection;

			selection = this.frame.state().get('selection');

			if ( parseInt( this.photo_id ) > 0 ) {
				selected = wp.media.attachment( this.photo_id );
				selection.add( selected );
			}
		},

		_create: function(src) {
			var attr = {'class': 'ppc-thumb', width: 150},
				css = {display: 'block', margin: '15px 0'};

			if ( src ) {
				attr.src = src;
			}

			return $('<img>').attr( attr ).css( css ).insertBefore( this.$el );
		},

		maybeRemovePhoto: function() {
			if ( '' === this.$el.val() ) {
				if ( this.img_holder.length ) {
					this.img_holder.remove();
					this.img_holder = false;
				}
			} else {
				if ( ! this.img_holder.length ) {
					this.img_holder = this._create( this.$el.val() );
				} else {
					this.img_holder.attr( 'src', this.$el.val() );
				}
			}

			this.url = this.$el.val();
		}
	});

	return new Photo();
}(jQuery,document));