== Profile Photo ==
Contributors: irenem
Requires at least: 3.7
Test up to: 4.7
Stable tag: 1.0.0
License: GPLv2 or higher

Custom profile photo. A good alternative for global gravatar.

== Description ==
Use custom profile photo to your author's profile page or use it as an alternative for global gravatar - sitewide! It's light, simple and easy to use. Whether self hosted or from external source, changing your users profile photo is made possible.

== Installation ==
1. Upload `profile-photo` to the `/wp-content/plugins/` directory then activate.
1. Go to `Your Profile` then set your custom profile photo. 

== Changelog ==

= 1.0.0 =
* First release -: