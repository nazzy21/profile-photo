<?php
/**
 * Plugin Name: Profile Photo
 * Description: Use custom profile photo to your author's profile page or use it as an alternative for global gravatar - sitewide! It's light, simple and easy to use. Whether self hosted or from external source photo, changing your users profile photo is made possible.
 * Author: Irene A. Mitchell
 * Author URI: http://www.irenemitchell.info
 * Version: 1.0.0
 * Text Domain: profile-photo
 **/
if ( ! defined( 'ABSPATH' ) ) :
	die( 'No direct access!' );
endif;

if ( ! class_exists( 'ProfilePicture' ) ) :
	class ProfilePicture {
		/**
		 * @var (int|string) Version controller of this mini-plugin.
		 **/
		var $version = '1.0.0';

		/**
		 * @var (bool) Wether to use profile photo in favor of avatar.
		 **/
		var $force_display = false;

		public function __construct() {
			if ( is_admin() ) {
				/**
				 * Add profile picture fields to edit profile.
				 *
				 * We are using `show_password_fields` filter but we are not really changing it's flow,
				 * we just want to add our own fields in the right area.
				 *
				 * @since 1.0.0
				 **/
				add_filter( 'show_password_fields', array( $this, 'fields' ), 10, 2 );

				/**
				 * Listen to update profile settings action and save the user preferences.
				 **/
				add_action( 'personal_options_update', array( $this, 'update' ) );
				add_action( 'edit_user_profile_update', array( $this, 'update' ) );
			}

			/**
			 * Let's checking user settings and change the `avatar` accordingly.
			 *
			 * Note: * If "Use as avatar" is on, profile photo will replace any instance call of `get_avatar`
			 * even if `show_avatar` option is disabled.
			 **/
			add_filter( 'pre_get_avatar', array( $this, 'pre_get_avatar' ), 10, 3 );
			add_filter( 'get_avatar', array( $this, 'get_avatar' ), 99, 6 );
		}

		function get_user_id( $id_or_email ) {
			if ( is_string( $id_or_email ) && is_email( $id_or_email ) ) {
					/**
					 * We need the user ID to get the metas. **/
					$user = get_user_by( 'email', $id_or_email );
					$id_or_email = $user->ID;
			}

			return $id_or_email;
		}

		/**
		 * Returns user's profile photo or null.
		 *
		 * @since 1.0.0
		 *
		 * @param (mixed) $id_or_email		Usually the $id_or_email param set on `get_avatar` instance.
		 * @pram (int) $size				The size of photo to produce.
		 * @param (array) $args				The `$args` param set from `get_avatar` instance.
		 **/
		function get_profile_photo( $id_or_email, $size, $alt, $args ) {
			$id_or_email = $this->get_user_id( $id_or_email );
			$profile_photo_id = get_user_meta( $id_or_email, 'profile_photo_id', true );
			$profile_photo = get_user_meta( $id_or_email, 'profile_photo_2', true );
			$class = array( 'avatar', 'avatar-' . $size, 'photo' );

			if ( ! empty( $args['class'] ) ) {
				array_push( $class, $args['class'] );
			}
			$class = implode( ' ', $class );

			if ( 0 < $profile_photo_id ) {
				$attr = array(
					'alt' => $alt,
					'class' => $class,
					'width' => $size,
					'height' => $size,
				);
				$profile_photo = wp_get_attachment_image( $profile_photo_id, array( $size, $size ), false, $attr );
			} elseif ( ! empty( $profile_photo ) ) {
				$profile_photo = sprintf( '<img src="%1$s" class="%2$s" alt="%3$s" width="%4$s" />', $profile_photo, $class, $alt, $size );
			}

			return $profile_photo;
		}

		function pre_get_avatar( $null, $id_or_email, $args ) {
			$user_id = $this->get_user_id( $id_or_email );

			if ( ! empty( $user_id ) ) {
				// Check if set to use site-wide
				$null = $this->force_display = get_user_meta( $user_id, 'profile_use_as_avatar', true );
			}

			return $null;
		}

		function get_avatar( $avatar, $id_or_email, $size, $default, $alt, $args ) {
			$replace = is_author();

			if ( $this->force_display || $replace ) {
				$profile_photo = $this->get_profile_photo( $id_or_email, $size, $alt, $args );

				if ( ! empty( $profile_photo ) ) {
					$avatar = $profile_photo;
				}
			}

			return $avatar;
		}

		function fields( $show, $userdata ) {
			$photo = get_user_meta( $userdata->ID, 'profile_photo_2', true );
			$photo_id = get_user_meta( $userdata->ID, 'profile_photo_id', true );
			$use_as_avatar = get_user_meta( $userdata->ID, 'profile_use_as_avatar', true );
			$use_as_avatar = ! empty( $use_as_avatar );
			$placeholder = __( 'Enter or choose profile photo', 'profile-photo' );

			$td = sprintf( '<input type="hidden" autocomplete="off" name="profile_photo_id" id="profile_photo_id" value="%s" />', (int) $photo_id );
			$td .= sprintf( '<input type="text" autocomplete="off" size="40" name="profile_photo_2" id="profile_photo_2" value="%1$s" placeholder="%2$s" />', esc_attr( $photo ), $placeholder );
			$td .= sprintf( '<input type="button" class="hide-if-no-js button browse" value="%s" />', __( 'Browse', 'profile-photo' ) );

			$use_as = '<p><label><input type="checkbox" name="profile_use_as_avatar" value="1" %1$s /> %2$s</label></p>';
			$td .= sprintf( $use_as, checked( true, $use_as_avatar, false ), __( 'Use sitewide as my avatar.', 'profile-photo' ) );
			$description = __( 'By default, your profile photo will show to your author\'s profile page. Tick the box aboved to use as your avatar.', 'profile-photo' );
			$td .= sprintf( '<p class="description">%s</p>', $description );

			printf( '<tr><th>%1$s</th><td>%2$s</td></tr>', __( 'Profile Photo', 'profile-photo' ), $td );

			/**
			 * Let's make sure that `wp_enqueue_media` is enqueued on this page.
			 **/
			wp_enqueue_media();

			/**
			 * Now that we are rendering the profile fields. Let's add the needed assets.
			 **/
			add_action( 'admin_footer', array( $this, 'include_assets' ) );

			return $show;
		}

		function update( $user_id ) {
			$profile_photo_id = ! empty( $_POST['profile_photo_id'] ) ? (int) $_POST['profile_photo_id'] : '';
			$profile_photo = ! empty( $_POST['profile_photo_2'] ) ? $_POST['profile_photo_2'] : '';
			$use_as_avatar = ! empty( $_POST['profile_use_as_avatar'] );

			if ( ! empty( $profile_photo_id ) ) {
				$attachment = wp_get_attachment_image_src( $profile_photo_id, 'full' );

				if ( ! empty( $attachment ) && $profile_photo !== $attachment[0] ) {
					/**
					 * If attachment src is NOT the same to $profile_photo, empty profile photo ID **/
					$profile_photo_id = '';
				}
			}

			update_user_meta( $user_id, 'profile_photo_id', $profile_photo_id );
			update_user_meta( $user_id, 'profile_photo_2', $profile_photo );
			update_user_meta( $user_id, 'profile_use_as_avatar', $use_as_avatar );
		}

		function include_assets() {
			$path = WP_PLUGIN_URL . '/profile-photo/';
			$version = $this->version;

			wp_enqueue_script( 'ppc-js', $path . 'author.min.js', array( 'jquery', 'underscore' ), $version );
			$array = array(
				'title' => __( 'Insert Profile Photo', 'profile-photo' ),
			);
			wp_localize_script( 'ppc-js', '_ppc', $array );
		}
	}
	new ProfilePicture();
endif;
