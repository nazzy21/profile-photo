/* global require */
var gulp = require( 'gulp' ),
	sourcemaps = require( 'gulp-sourcemaps' ),
	header = require( 'gulp-header' ),
	date = new Date(),
	datestring = date.toDateString(),
	fs = require( 'fs' ),
	argv = require( 'yargs' ).argv,
	pkg = JSON.parse(fs.readFileSync('./package.json')),
	banner_args = {
		title: pkg.title,
		version: pkg.version,
		datestring: datestring,
		author: pkg.author
	},
	banner = '/**! <%=title%> -v <%=version%>\n' +
	' * Copyright (c) <%=datestring%>\n' +
	' * Author <%=author%>\n' +
	' * License GPvL2 or higher\n' +
	'!**/\n',
	php_files = [ '*.php' ],
	js_files = [ '*.js', '!*.min.js'];

gulp.task( 'phplint', function() {
	var phplint = require( 'gulp-phplint' );

	gulp.src( php_files ).pipe(phplint());
});

gulp.task( 'phpcs', function() {
	var phpcs = require( 'gulp-phpcs' ),
		options = {
			// Change this to your actual phpcs location
			bin: '../../../../../phpcs/scripts/phpcs',
			standard: 'WordPress-Core'
		};

	if ( argv.src ) {
		if ( 1 === argv.src ) {
			options.bin = '../../../../vendor/bin/phpcs';
		} else {
			options.bin = '../../../../../vendor/bin/phpcs';
		}
	}

	gulp.src( php_files ).pipe( phpcs( options ) ).pipe(phpcs.reporter('log'));
});

gulp.task( 'jsvalidate', function() {
	var jsvalidate = require( 'gulp-jsvalidate' );

	gulp.src(js_files).pipe(jsvalidate());
});

gulp.task( 'jshint', function() {
	var jshint = require( 'gulp-jshint' ),
		options = {
			curly:   true,
			browser: true,
			eqeqeq:  true,
			immed:   true,
			latedef: true,
			newcap:  true,
			noarg:   true,
			sub:     true,
			undef:   true,
			boss:    true,
			eqnull:  true,
			unused:  true,
			quotmark: 'single',
			predef: [ 'jQuery', 'Backbone', '_' ],
			globals: {
				exports: true,
				module:  false
			}
		};

	gulp.src( js_files ).pipe(jshint(options)).pipe(jshint.reporter('default'));
});

gulp.task( 'js', function() {
	// Minified JS files
	var minify_js = ['*.js', '!*.min.js', '!*Gulpfile.js'],
		concat = require( 'gulp-concat' ),
		replace = require( 'gulp-replace' ),
		rename = require( 'gulp-rename' ),
		uglify = require( 'gulp-uglify' );

	gulp.src( minify_js )
		.pipe(sourcemaps.init())
		.pipe(uglify({preserveComments:'license'}))
		.pipe(rename(function( path ) {
			path.basename = path.basename + '.min';
		}))
		.pipe(header(banner, banner_args))
		.pipe(sourcemaps.write('maps'))
		.pipe(gulp.dest('./'));
});

gulp.task( 'watch', ['js'], function() {
	gulp.watch( ['assets/js/*.js', 'assets/js/**/*.js', '!assets/js/*.min.js'], ['js'] );
});

gulp.task( 'makepot', function() {
	var wpPot = require( 'gulp-wp-pot' ),
		name = pkg.title.toLowerCase().replace( / /g, '-' );

	gulp.src( php_files )
		.pipe(wpPot({package: name + ' ' + pkg.version}))
		.pipe(gulp.dest('languages/' + name + '-en_US.pot'));
});

gulp.task( 'generate-zip', function() {
	var zip = require( 'gulp-zip' ),
		notify = require('gulp-notify'),
		zipfile = pkg.title.toLowerCase().replace( / /g, '-' ) + pkg.version + '.zip',
		files = [
			'*',
			'**',
			'!logs/*',
			'!logs/**',
			'!logs',
			'!docs/*',
			'!docs/**',
			'!docs',
			'!node_modules/*',
			'!node_modules/**',
			'!node_modules',
			'!.git/*',
			'!.git/**',
			'!.git',
			'!.gitignore',
			'!Gulpfile.js',
			'!Gruntfile.js',
			'!package.json',
			'!.sass-cache',
			'!maps/*',
			'!maps/**',
			'!maps',
			'!temp/*',
			'!temp/**',
			'!temp',
			'!tests/*',
			'!tests/**',
			'!tests',
			'!README.md'
		];

	gulp.src( files, {base: '../'})
		.pipe(zip(zipfile))
		.pipe(gulp.dest('../'))
		.pipe(notify({message: zipfile + ' file successfully generated!', onLast:true}));
});